<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/medoc/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'medoc_description' => 'Patch unifiant le comportement et la syntaxe d\'insertion des documents, pour soigner l\'inconstance d\'affichage dont souffrent les documents de SPIP. Désormais, c\'est vous qui décidez !

- Le raccourci &lt;doc&gt; affiche toujours une vignette cliquable
- Syntaxe d\'insertion unifiée : <docXXX|left>, <docXXX|img|right>, <docXXX|emb|center>
- Rétrocompatibilité préservée',
	'medoc_nom' => 'Modèle &lt;doc&gt; unifié',
	'medoc_slogan' => 'L\'insertion de documents simplifiée !'
);

?>